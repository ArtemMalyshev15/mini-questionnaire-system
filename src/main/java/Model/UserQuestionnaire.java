package Model;

public class UserQuestionnaire {

    private String login;
    private int id_Questionnaire;
    private String user_Answers;

    public UserQuestionnaire(String login, int id_Questionnaire, String user_Answers) {
        this.login = login;
        this.id_Questionnaire = id_Questionnaire;
        this.user_Answers = user_Answers;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public int getId_Questionnaire() {
        return id_Questionnaire;
    }

    public void setId_Questionnaire(int id_Questionnaire) {
        this.id_Questionnaire = id_Questionnaire;
    }

    public String getUser_Answers() {
        return user_Answers;
    }

    public void setUser_Answers(String user_Answers) {
        this.user_Answers = user_Answers;
    }
}
