package Model;

public class QuestionnaireQuestion {

    private int id_Questionnaire;
    private int id_Question;

    public QuestionnaireQuestion(int id_Questionnaire, int id_Question) {
        this.id_Questionnaire = id_Questionnaire;
        this.id_Question = id_Question;
    }

    public int getId_Questionnaire() {
        return id_Questionnaire;
    }

    public void setId_Questionnaire(int id_Questionnaire) {
        this.id_Questionnaire = id_Questionnaire;
    }

    public int getId_Question() {
        return id_Question;
    }

    public void setId_Question(int id_Question) {
        this.id_Question = id_Question;
    }
}
