package Model;

public class Answer {

    private int id_Answer;
    private int id_Question;
    private String answer_Wording;

    public Answer(int id_Answer, int id_Question, String answer_Wording) {
        this.id_Answer = id_Answer;
        this.id_Question = id_Question;
        this.answer_Wording = answer_Wording;
    }

    public int getId_Answer() {
        return id_Answer;
    }

    public void setId_Answer(int id_Answer) {
        this.id_Answer = id_Answer;
    }

    public int getId_Question() {
        return id_Question;
    }

    public void setId_Question(int id_Question) {
        this.id_Question = id_Question;
    }

    public String getAnswer_Wording() {
        return answer_Wording;
    }

    public void setAnswer_Wording(String answer_Wording) {
        this.answer_Wording = answer_Wording;
    }
}
