package Model;

public class Questionnaire {

    private int id_Questionnaire;
    private String name_Questionnaire;

    public Questionnaire(int id_Questionnaire, String name_Questionnaire) {
        this.id_Questionnaire = id_Questionnaire;
        this.name_Questionnaire = name_Questionnaire;
    }

    public int getId_Questionnaire() {
        return id_Questionnaire;
    }

    public void setId_Questionnaire(int id_Questionnaire) {
        this.id_Questionnaire = id_Questionnaire;
    }

    public String getName_Questionnaire() {
        return name_Questionnaire;
    }

    public void setName_Questionnaire(String name_Questionnaire) {
        this.name_Questionnaire = name_Questionnaire;
    }
}
