package Model;

public class Question {

    private int id_Question;
    private int count_Answers;
    private String question_Wording;

    public Question(int id_Question, int count_Answers, String question_Wording) {
        this.id_Question = id_Question;
        this.count_Answers = count_Answers;
        this.question_Wording = question_Wording;
    }

    public int getId_Question() {
        return id_Question;
    }

    public void setId_Question(int id_Question) {
        this.id_Question = id_Question;
    }

    public int getCount_Answers() {
        return count_Answers;
    }

    public void setCount_Answers(int count_Answers) {
        this.count_Answers = count_Answers;
    }

    public String getQuestion_Wording() {
        return question_Wording;
    }

    public void setQuestion_Wording(String question_Wording) {
        this.question_Wording = question_Wording;
    }
}
