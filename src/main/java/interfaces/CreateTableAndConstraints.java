package interfaces;

import java.sql.SQLException;

public interface CreateTableAndConstraints {

    void createTable() throws SQLException;
    void createForeignKeys() throws SQLException;
}
