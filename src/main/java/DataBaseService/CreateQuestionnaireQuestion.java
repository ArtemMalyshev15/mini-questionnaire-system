package DataBaseService;

import interfaces.CreateTableAndConstraints;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class CreateQuestionnaireQuestion extends ServiceTable implements CreateTableAndConstraints {

    public CreateQuestionnaireQuestion() throws SQLException {
        super("questionnaire_question");
    }

    @Override
    public void createTable() throws SQLException {
        super.executeSQLScript("CREATE TABLE IF NOT EXISTS questionnaire_question (" +
                "id_questionnaire INTEGER," +
                "id_question INTEGER," +
                "PRIMARY KEY (id_questionnaire, id_question))");
    }

    @Override
    public void createForeignKeys() throws SQLException {
        super.executeSQLScript("ALTER TABLE questionnaire_question ADD FOREIGN KEY (id_questionnaire)" +
                "REFERENCES questionnaire(id_questionnaire) ON DELETE CASCADE ON UPDATE CASCADE");
        super.executeSQLScript("ALTER TABLE questionnaire_question ADD FOREIGN KEY (id_question)" +
                "REFERENCES question(id_question) ON DELETE CASCADE ON UPDATE CASCADE");
    }

    public void addQuestionIntoQuestionnaire(int id_questionnaire, int id_question) throws SQLException {
        try (PreparedStatement preparedStatement = DataBaseConnection.getConnection()
                .prepareStatement("INSERT INTO questionnaire_question VALUES (?, ?)")) {
            preparedStatement.setInt(1, id_questionnaire);
            preparedStatement.setInt(2, id_question);
            preparedStatement.executeUpdate();
        }
    }

    public void deleteQuestionFromQuestionnaire(int id_questionnaire, int id_question) throws SQLException {
        try (PreparedStatement preparedStatement = DataBaseConnection.getConnection()
                .prepareStatement("DELETE FROM questionnaire_question " +
                        "WHERE id_questionnaire = ? AND id_question = ?")) {
            preparedStatement.setInt(1, id_questionnaire);
            preparedStatement.setInt(2, id_question);
            preparedStatement.executeUpdate();
        }
    }
}
