package DataBaseService;

import Model.UserQuestionnaire;
import interfaces.CreateTableAndConstraints;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CreateUserQuestionnaire extends ServiceTable implements CreateTableAndConstraints {

    public CreateUserQuestionnaire() throws SQLException {
        super("user_questionnaire");
    }

    @Override
    public void createTable() throws SQLException {
        super.executeSQLScript("CREATE TABLE IF NOT EXISTS user_questionnaire (" +
                "login VARCHAR(20)," +
                "id_questionnaire INTEGER," +
                "user_answers VARCHAR(255) NOT NULL," +
                "PRIMARY KEY (login, id_questionnaire))");
    }

    @Override
    public void createForeignKeys() throws SQLException {
        super.executeSQLScript("ALTER TABLE user_questionnaire ADD FOREIGN KEY (login) " +
                "REFERENCES users(login) ON DELETE CASCADE ON UPDATE CASCADE");
        super.executeSQLScript("ALTER TABLE user_questionnaire ADD FOREIGN KEY (id_questionnaire)" +
                "REFERENCES questionnaire(id_questionnaire) ON DELETE CASCADE ON UPDATE CASCADE");
    }

    public void addUserAnswers(UserQuestionnaire userQuestionnaire) throws SQLException {
        try (PreparedStatement preparedStatement = DataBaseConnection.getConnection()
                .prepareStatement("INSERT INTO user_questionnaire VALUES (?, ?, ?)")) {
            preparedStatement.setString(1, userQuestionnaire.getLogin());
            preparedStatement.setInt(2, userQuestionnaire.getId_Questionnaire());
            preparedStatement.setString(3, userQuestionnaire.getUser_Answers());
            preparedStatement.executeUpdate();
        }
    }

    public String getUserAnswers(UserQuestionnaire userQuestionnaire) throws SQLException {
        try (ResultSet resultSet = DataBaseConnection.getConnection().createStatement()
                .executeQuery("SELECT user_answers FROM user_questionnaire " +
                        "WHERE login = \'" + userQuestionnaire.getLogin() +
                        "\' AND id_questionnaire = " + userQuestionnaire.getId_Questionnaire())) {
            resultSet.next();
            return resultSet.getString(1);
        }
    }

    //todo: получить список пользователей сделать тут или перенести к пользователю?
    public String getUsers() throws SQLException {
        try (ResultSet resultSet = DataBaseConnection.getConnection().createStatement()
                .executeQuery("SELECT login FROM user_questionnaire")) {
            String users = "";
            while (resultSet.next()) {
                users += resultSet.getString(1) + "@";
            }
            return users;
        }
    }
}
