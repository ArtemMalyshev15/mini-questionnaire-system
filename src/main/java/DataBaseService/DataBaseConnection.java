package DataBaseService;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataBaseConnection {

    public static final String JDBC_DRIVER = "org.h2.Driver";
    public static final String DB_URL = "jdbc:h2:~/MiniQuestionnaireSystem";

    public CreateAnswer createAnswer;
    public CreateQuestion createQuestion;
    public CreateQuestionnaire createQuestionnaire;
    public CreateQuestionnaireQuestion createQuestionnaireQuestion;
    public CreateUser createUser;
    public CreateUserQuestionnaire createUserQuestionnaire;

    public DataBaseConnection() throws ClassNotFoundException, SQLException {
        Class.forName(JDBC_DRIVER);

        createAnswer = new CreateAnswer();
        createQuestion = new CreateQuestion();
        createQuestionnaire = new CreateQuestionnaire();
        createQuestionnaireQuestion = new CreateQuestionnaireQuestion();
        createUser = new CreateUser();
        createUserQuestionnaire = new CreateUserQuestionnaire();
    }

    public void createTablesAndForeignKeys() throws SQLException {
        createUser.createTable();
        createQuestionnaire.createTable();
        createQuestion.createTable();
        createAnswer.createTable();
        createUserQuestionnaire.createTable();
        createUserQuestionnaire.createForeignKeys();
        createQuestionnaireQuestion.createTable();
        createQuestionnaireQuestion.createForeignKeys();
        createAnswer.createTable();
        createAnswer.createForeignKeys();
    }

    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(DB_URL);

    }
}
