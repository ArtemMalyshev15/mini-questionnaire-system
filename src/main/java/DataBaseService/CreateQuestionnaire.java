package DataBaseService;

import interfaces.CreateTableAndConstraints;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CreateQuestionnaire extends ServiceTable implements CreateTableAndConstraints {

    public CreateQuestionnaire() throws SQLException {
        super("questionnaire");
    }

    @Override
    public void createTable() throws SQLException {
        super.executeSQLScript("CREATE TABLE IF NOT EXISTS questionnaire (" +
                "id_questionnaire INTEGER AUTO_INCREMENT PRIMARY KEY," +
                "name_questionnaire VARCHAR(70) NOT NULL)");
    }

    @Override
    public void createForeignKeys() {

    }

    public void addQuestionnaire(String name) throws SQLException {
        try (PreparedStatement preparedStatement = DataBaseConnection.getConnection()
                .prepareStatement("INSERT INTO questionnaire (name_questionnaire) VALUES (?)")) {
            preparedStatement.setString(1, name);
            preparedStatement.executeUpdate();
        }
    }

    public String getAllQuestionnaire() throws SQLException {
        try(ResultSet resultSet = DataBaseConnection.getConnection()
                .createStatement().executeQuery("SELECT * FROM questionnaire")) {
            String result = "";
            while (resultSet.next()) {
                result += resultSet.getString(2);
            }
            return result;
        }
    }

    public int getQuestionnaireId(String name) throws SQLException {
        try (ResultSet resultSet = DataBaseConnection.getConnection()
                .createStatement().executeQuery("SELECT id_questionnaire " +
                        "FROM questionnaire WHERE name_questionnaire = \'" + name + "\'")) {
            resultSet.next();
            return resultSet.getInt(1);
        }
    }
}
