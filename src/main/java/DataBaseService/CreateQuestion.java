package DataBaseService;

import interfaces.CreateTableAndConstraints;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CreateQuestion extends ServiceTable implements CreateTableAndConstraints {

    public CreateQuestion() throws SQLException {
        super("question");
    }

    @Override
    public void createTable() throws SQLException {
        super.executeSQLScript("CREATE TABLE IF NOT EXISTS question (" +
                "id_question INTEGER AUTO_INCREMENT PRIMARY KEY," +
                "wording_question VARCHAR(255) NOT NULL UNIQUE," +
                "count_answers INTEGER NOT NULL)");
    }

    @Override
    public void createForeignKeys() {

    }

    public void addQuestion(String formulation, int countAnswers) throws SQLException {
        try (PreparedStatement preparedStatement = DataBaseConnection.getConnection()
                .prepareStatement("INSERT INTO question (wording_question, count_answers) " +
                        "VALUES (?, ?) ")) {
            preparedStatement.setString(1, formulation);
            preparedStatement.setInt(2, countAnswers);
            preparedStatement.executeUpdate();
        }
    }

    public void deleteQuestion(String formulation) throws SQLException {
        try (PreparedStatement preparedStatement = DataBaseConnection.getConnection()
                .prepareStatement("DELETE FROM question WHERE wording_question = ?")) {
            preparedStatement.setString(1, formulation);
        }
    }

    public int getQuestionIdByFormulation(String formulation) throws SQLException {
        try (ResultSet resultSet = DataBaseConnection.getConnection().createStatement()
                .executeQuery("SELECT id_question FROM question " +
                        "WHERE wording_question = \'"+ formulation+ "\'")) {
            resultSet.next();
            return resultSet.getInt(1);
        }
    }

    public String getQuestionFormulationById (int id_Question) throws SQLException {
        try (ResultSet resultSet = DataBaseConnection.getConnection().createStatement()
                .executeQuery("SELECT wording_question FROM question " +
                        "WHERE id_question = " + id_Question)) {
            resultSet.next();
            return resultSet.getString(1);
        }
    }

    public int getCountAnswersForQuestionByFormulation (String formulation) throws SQLException {
        try (ResultSet resultSet = DataBaseConnection.getConnection().createStatement()
                .executeQuery("SELECT count_answers FROM question " +
                        "WHERE  wording_question = \'" + formulation + "\'")) {
            resultSet.next();
            return resultSet.getInt(1);
        }
    }
}
