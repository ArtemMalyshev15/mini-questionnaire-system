package DataBaseService;

import interfaces.CreateTableAndConstraints;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CreateAnswer extends ServiceTable implements CreateTableAndConstraints {

    public CreateAnswer() throws SQLException {
        super("answer");
    }

    @Override
    public void createTable() throws SQLException {
        super.executeSQLScript("CREATE TABLE IF NOT EXISTS answer (" +
                "id_Answer INTEGER AUTO_INCREMENT," +
                "id_Question INTEGER," +
                "answer_Wording VARCHAR(255)," +
                "PRIMARY KEY (id_Answer, id_Question))");
    }

    @Override
    public void createForeignKeys() throws SQLException {
        super.executeSQLScript("ALTER TABLE answer ADD FOREIGN KEY (id_Question) " +
                "REFERENCES question(id_Question) ON DELETE CASCADE ON UPDATE CASCADE");
    }

    public void addAnswer(int id_question, String formulation) throws SQLException {
        try (PreparedStatement preparedStatement = DataBaseConnection.getConnection()
                        .prepareStatement("INSERT INTO answer (id_Question, answer_Wording) VALUES (?, ?)")) {
            preparedStatement.setInt(1, id_question);
            preparedStatement.setString(2, formulation);
            preparedStatement.executeUpdate();
        }
    }

    public void deleteAnswerInQuestion(int id_question) throws SQLException {
        try (PreparedStatement preparedStatement = DataBaseConnection.getConnection()
                .prepareStatement("DELETE FROM answer WHERE id_Question = ?")) {
            preparedStatement.setInt(1, id_question);
            preparedStatement.executeUpdate();
        }
    }

    public int getAnswerId(String formulation, int id_Question) throws SQLException {
        try (ResultSet resultSet = DataBaseConnection.getConnection().createStatement()
                .executeQuery("SELECT id_answer FROM answers " +
                        "WHERE answer_Wording = \'" + formulation + "\' AND id_Question = " + id_Question)) {
            resultSet.next();
            return resultSet.getInt(1);
        }
    }

    public String getAnswerFormulation(int id_answer) throws SQLException {
        try (ResultSet resultSet = DataBaseConnection.getConnection().createStatement()
                .executeQuery("SELECT answer_Wording FROM answers WHERE id_answer = " + id_answer)) {
            resultSet.next();
            return resultSet.getString(1);
        }
    }

    public int getQuestionIdByAnswerId(int id_answer) throws SQLException {
        try (ResultSet resultSet = DataBaseConnection.getConnection().createStatement()
                .executeQuery("SELECT id_Question FROM answer WHERE id_answer = " + id_answer)) {
            resultSet.next();
            return resultSet.getInt(1);
        }
    }
}
