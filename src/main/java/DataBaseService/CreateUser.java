package DataBaseService;

import Model.User;
import interfaces.CreateTableAndConstraints;

import java.sql.*;

public class CreateUser extends ServiceTable implements CreateTableAndConstraints {

    public CreateUser() throws SQLException {
        super("users");
    }

    @Override
    public void createTable() throws SQLException {
        super.executeSQLScript("CREATE TABLE IF NOT EXISTS users (" +
                "login VARCHAR(20) PRIMARY KEY, " +
                "password VARCHAR(20) NOT NULL, " +
                "role tinyint NOT NULL)");
    }

    @Override
    public void createForeignKeys() {

    }

    public void addUser(User user) throws SQLException {
        try(PreparedStatement preparedStatement = DataBaseConnection.getConnection()
                .prepareStatement("INSERT INTO users VALUES (?, ?, ?)")) {
            preparedStatement.setString(1, user.getLogin());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setInt(3, user.getRole());
        }
    }

    public int findUser(User user) throws SQLException {
        try(PreparedStatement preparedStatement = DataBaseConnection.getConnection()
                .prepareStatement("SELECT * FROM users WHERE login = ? and password = ?")){
            preparedStatement.setString(1,user.getLogin());
            preparedStatement.setString(2, user.getPassword());
            ResultSet resultSet = preparedStatement.executeQuery();
            if (!resultSet.isBeforeFirst()){
                return -1;
            }
            else {
                resultSet.next();
                return resultSet.getInt(3);
            }
        }
    }
}
