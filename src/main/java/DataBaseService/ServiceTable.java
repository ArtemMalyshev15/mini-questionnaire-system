package DataBaseService;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class ServiceTable {

    public Connection connection;
    public String tableName;

    public ServiceTable(String tableName) throws SQLException {
        this.tableName = tableName;
        connection = DataBaseConnection.getConnection();
    }

    public void executeSQLScript(String sqlScript) throws SQLException {
        try {
            if (connection == null || connection.isClosed()) {
                connection = DataBaseConnection.getConnection();
            }
            Statement statement = connection.createStatement();
            statement.executeUpdate(sqlScript);
            statement.close();
        }
        finally {
            connection.close();
        }
    }
}
